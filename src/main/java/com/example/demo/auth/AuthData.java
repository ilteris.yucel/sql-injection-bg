package com.example.demo.auth;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Data
@ToString
@Getter
@Setter
public class AuthData {
    private String usernameOrEmail;
    private String password;

}
