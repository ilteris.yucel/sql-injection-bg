package com.example.demo.auth;
import com.example.demo.user.User;
import jakarta.persistence.NoResultException;
import jakarta.persistence.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import jakarta.persistence.EntityManager;
import org.json.JSONException;
import org.json.JSONObject;


@RestController
public class AuthController {

    private final EntityManager entityManager;

    AuthController(final EntityManager entityManager){
        this.entityManager = entityManager;
    }

    @PostMapping(path = "/protected-auth", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> protectedAuth(@RequestBody AuthData authData) throws JSONException {
        JSONObject resp = new JSONObject();
        String query = "SELECT * FROM user WHERE username =?1 OR email =?2";
        Query q = entityManager.createNativeQuery(query, User.class);
        q.setParameter(1, authData.getUsernameOrEmail());
        q.setParameter(2, authData.getUsernameOrEmail());
        try{
            User user = (User) q.getSingleResult();
            if (!user.getPassword().equals(authData.getPassword())){
                resp.put("message", "Wrong Password");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resp.toString());
            }
            resp.put("message", "Login Successful!");
            return ResponseEntity.status(HttpStatus.OK).body(resp.toString());
        }catch (NoResultException e){
            resp.put("message", "User not Found!");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resp.toString());
        }

    }

    @PostMapping(path = "/unprotected-auth", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> unprotectedAuth(@RequestBody AuthData authData) throws JSONException {
        JSONObject resp = new JSONObject();
        String usernameOrEmail = authData.getUsernameOrEmail();
        String password = authData.getPassword();
        String query = "SELECT * FROM user WHERE username = " +  "'" + usernameOrEmail + "'" + " AND password= " + "'" + password + "'";
        Query q = entityManager.createNativeQuery(query, User.class);
        try{
            User user = (User) q.getSingleResult();
            resp.put("message", "Login Successful!");
            return ResponseEntity.status(HttpStatus.OK).body(resp.toString());
        }catch (NoResultException e){
            resp.put("message", "User not Found!");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resp.toString());
        }

    }
}
