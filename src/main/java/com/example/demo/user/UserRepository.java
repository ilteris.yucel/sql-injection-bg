package com.example.demo.user;
import com.example.demo.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

@Repository
@Transactional
public interface UserRepository extends CrudRepository<User, Integer> {
    User findUserById(Integer id);
    User findUserByUsername(String username);
    User findUserByEmail(String email);

    @Modifying
    @Query("UPDATE User u set u.username = :username WHERE u.id = :id")
    void updateUsername(@Param("id") int id, @Param("username") String username);

    @Modifying
    @Query("UPDATE User u set u.email = :email WHERE u.id = :id")
    void updateEmail(@Param("id") int id, @Param("email") String email);

    @Modifying
    @Query("UPDATE User u set u.password = :password WHERE u.id = :id")
    void updatePassword(@Param("id") int id, @Param("password") String password);

    @Modifying
    @Query(value = "DELETE FROM User u WHERE u.id = :id")
    int deleteUserById(@Param("id") int id);
    @Modifying
    @Query(value = "DELETE FROM User u WHERE u.username = :username")
    int deleteUserByUsername(@Param("username") String username);

    @Modifying
    @Query(value = "DELETE FROM User u WHERE u.email = :email")
    int deleteUserByEmail(@Param("email") String email);
}
