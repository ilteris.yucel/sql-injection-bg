package com.example.demo.user;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    private final UserRepository userRepository;

    UserController(final UserRepository userRepository){
        this.userRepository = userRepository;
    }


    @PostMapping("/user/add")
    @ResponseStatus(HttpStatus.CREATED)
    public String addCustomer(@RequestBody User newUser) {
        User user = new User();
        user.setUsername(newUser.getUsername());
        user.setEmail(newUser.getEmail());
        user.setPassword(newUser.getPassword());
        userRepository.save(user);
        return user.toString();
    }

    @GetMapping("/user/list")
    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }

    @GetMapping("/user/find/id/{id}")
    public User findUserById(@PathVariable Integer id) {
        return userRepository.findUserById(id);
    }

    @GetMapping("/user/find/username/{username}")
    public User findUserByUsername(@PathVariable String username) {
        return userRepository.findUserByUsername(username);
    }

    @GetMapping("/user/find/email/{email}")
    public User findUserByEmail(@PathVariable String email) {
        return userRepository.findUserByEmail(email);
    }

    @PutMapping("/user/update")
    public User update(@RequestBody User newUserData) {
        User updatedUser = userRepository.findUserById(newUserData.getId());
        updatedUser.setUsername(newUserData.getUsername());
        updatedUser.setEmail(newUserData.getEmail());
        updatedUser.setPassword(newUserData.getPassword());
        userRepository.save(updatedUser);
        return updatedUser;
    }

    @DeleteMapping("/user/delete")
    public int deleteUserById(@RequestBody User deleteUser) {
        userRepository.delete(deleteUser);
        return deleteUser.getId();
    }

    @DeleteMapping("/user/delete/id/{id}")
    public int deleteUserById(@PathVariable int id) {
        return userRepository.deleteUserById(id);
    }

    @DeleteMapping("/user/delete/username/{username}")
    public int deleteUserByUsername(@PathVariable String username) {
        return userRepository.deleteUserByUsername(username);
    }

    @DeleteMapping("/user/delete/email/{email}")
    public int deleteUserByUserEmail(@PathVariable String email) {
        return userRepository.deleteUserByEmail(email);
    }
}
